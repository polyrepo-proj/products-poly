# Main

In the main branch the simplest form of code sits.
No extraction of common code is used to create a generic, re-usable jobs.

# Code-extraction-ci-templates-jobs

In the code-extraction-ci-templates-jobs branch the common code from the pipeline is extracted and placed into ci-templates-poly project. 
Then each microservice is re-using it in its pipeline using include: attribute.

Lesson 7 - 9 - Extract common logic (Job Templates - Part 1) - 54

# K8S

In the K8S branch the microservices are being deployed into K8S cluster.